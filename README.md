# Mise en forme et exploitation des fichiers décès INSEE

L'INSEE rend public les fichiers des personnes décédés depuis 1970 : https://insee.fr/fr/information/4190491

Les fichiers sont mis à jour mensuellement et contiennent les informations suivantes : nom, prénoms, sexe, date de naissance, code et libellé du lieu de naissance, date du décès, code du lieu de décès et numéro de l’acte de décès. Les fichiers sources sont au format CSV (séparateur virgule, encodage UTF-8).

Les scripts python et R de ce répertoire proposent les fonctionnalités suivantes :
- compiler et nettoyer les fichiers annuels et mensuels en un fichier unique pour faciliter les opérations de record linkage avec d'autres sources de données
- rechercher des correspondances dans la base INSEE à partir d'un fichier local
- visualiser les enregistrements (nombre de décès hebdomadaires)

## Compilation des fichiers annuels en un fichier unique et préparation des champs

1. Lecture des fichiers CSV sources

2. Compilation en un fichier unique

3. Nettoyage
	- séparation des noms et prénoms
	- nettoyage des chaînes de caractères
	- concaténation prenom_1 et prenom_2

4. enregistrement au format csv, Rds

## Recherche des correspondances (Record Linkage)

1. Chargement des données
	- données locales au format CSV, nettoyage des chaînes de caractères (nom et prénom)
	- données décès INSEE

2. Recherche des enregistrements candidats : données locales/insee avec au moins un champ en commun (nom ou date de naissance)

3. Calcul des distances de Damerau-Levenshtein entre :
	- nom
	- prenom vs prenom_1
	- prenom vs prenom_1_2
	- date de naissance

4. Sélection des correspondances avec distances <= distance maximale autorisée

5. Export des correspondances au format CSV

### TODO

- [ ] interface graphique record linkage
- [ ] fichier compilation R : corriger bug prenom 2
- [ ] ajouter les nouveaux fichiers de décès lors de leur mise à dispo sur INSEE / Data.gouv