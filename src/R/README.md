# Mise en forme et exploitation des fichiers décès INSEE

## Compilation des fichiers annuels en un fichier unique et préparation des champs pour la recherche de correspondances

**compilation_fichiers_deces_insee.R**

1. Lecture des fichiers CSV sources

2. Compilation en un fichier unique

3. Nettoyage
	- séparation des noms et prénoms
	- nettoyage des chaînes de caractères
	- concaténation prenom_1 et prenom_2

4. enregistrement au format csv, Rds

## Recherche des correspondances (Record Linkage)

**record_linkage_insee.R**

1. Chargement des données
	- données locales au format CSV, nettoyage des chaînes de caractères (nom et prénom)
	- données décès INSEE


2. Recherche des enregistrements candidats : données locales/insee avec au moins un champ en commun (nom ou date de naissance)

3. Calcul des distances de Damerau-Levenshtein entre :
	- nom
	- prenom vs prenom_1
	- prenom vs prenom_1_2
	- date de naissance


4. Sélection des correspondances avec distances <= distance maximale autorisée

5. Export des correspondances au format CSV

## Mise en oeuvre

**perform_record_linkage.R**

### TODO

- [ ] interface graphique record linkage
- [ ] fichier compilation R : corriger bug prenom 2
- [ ] ajouter les nouveaux fichiers de décès lors de leur mise à dispo sur INSEE / Data.gouv