# Record Linkage

Le script **record_linkage** permet de chercher des correspondances entre deux fichiers contenant des personnes.<br />Le script **record_linkage_insee** permet de chercher des correspondances entre un fichier contenant des personnes et le fichier de décès INSEE.

Les fichiers sources doivent être au format csv (séparateur : ";") et contenir les champs suivants :

| champs | type | description |
| -------- | -------- | -------- | 
| id     | numerique     | Identifiant unique de la personne dans la base locale |
| nom  | chaîne de caractères  | Nom de naissance |
| prenom  | chaîne de caractères  |  |
| date_naissance  | chaîne de caractères au format JJ/MM/AAAA  |  |

Les scripts exécutent les fonctions suivantes :

1) Chargement des données au format csv : id, nom, prenom, date de naissance<br />
2) Indexation des enregistrements à comparer<br />
3) Comparaison des enregistrements à partir d'algorithmes de matching (ici, comparaison de chaînes de caractères avec la distance de damerau_levenshtein)<br />$
4) Sélection des matchings avec score total >= 3
4) Export des résultats au format csv<br />

## TODO

- [ ] vérifier que les fichiers d'entrée contiennent bien les champs id, nom, prénom, date de naissance
- [ ] supprimer les espaces en milieu de chaîne
- [ ] nom de type 'AAAA BBBB' => générer une liste avec ['AAAA', 'BBBB'] pour comparer
- [ ] proposer un log (nombre de candidats, distribution du score, nombre de matchs avec score >= seuil, nombre de matchs par entrée)
- [ ] script de compilation des fichiers sources INSEE (déjà disponible en R [ici](https://gitlab.com/antoinelamer/fichiers_deces_insee/-/blob/master/src/R/compilation_fichiers_deces_insee.R))
- [ ] proposer des champs supplémentaires en sortie (lieu de naissance et de décès [INSEE])
- [ ] interface graphique
- [ ] classe matching
- [ ] paramètrage : log, seuil
- [ ] documentation, tuto
