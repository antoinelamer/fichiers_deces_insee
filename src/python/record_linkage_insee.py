# ----------------------------------------------------------------------
#
#
#
#
# ----------------------------------------------------------------------

import recordlinkage
from recordlinkage.preprocessing import clean
import pandas as pd
import path

try:
    import cPickle as pickle
except ModuleNotFoundError:
    import pickle

    
    
    
def recordlinkage_insee(local_file, export_file, maximal_allowed_distance = 3):
    """Compare the records of a local file
    with records from INSEE death files.
    Parameters
    ----------
    local_file : str
        Path and name of the local file.
    export_file : str
        Path and name of the export file with candidates linked records.
    maximal_allowed_distance : int
        Maximal allowed distance betweeen one field of the local file
        and the corresponding field of INSEE file to keep the record.
    """
    
    # Load databases
    local_data = load_local_database(local_file)
    insee_data = load_insee_data()
    print_data_summary(local_data, 'Local df')
    print_data_summary(insee_data, 'INSEE df')
    
    # Index
    candidates = index(local_data, insee_data)
    
    # Look for matches
    matches = compare(candidates, local_data, insee_data)
    
    # Select records
    selected_matches = select(matches, maximal_allowed_distance)
    
    # Export results
    export(selected_matches, export_file, local_data, insee_data)

    
def load_local_database(local_file):
    """Load the local file to be compared with INSEE death files.
    Parameters
    ----------
    local_file : str
        Path and name of the local file.
    """
    # Lecture du fichier
    local_data = pd.read_csv(local_file, sep = ";")
    local_data = local_data.add_suffix('_local')
    
    #, index_col = ['id']
    # Sauvegarde des champs sources
    local_data['nom_src_local'] = local_data['nom_local']    
    local_data['prenom_src_local'] = local_data['prenom_local']    
    
    # Nettoyage, chaînes de caractères
    local_data['nom_local'] = clean_string(local_data['nom_local'])
    local_data['prenom_local'] = clean_string(local_data['prenom_local'])
    
    local_data.index.names = ['index_local']
    
    number_local_rows = local_data.shape[0]
    print('Local data : ' + str(number_local_rows) + " rows")
        
    return(local_data)


# Nettoyage d'une chaîne de caractères
def clean_string(input_string):
    """Clean a string field.
    Parameters
    ----------
    input_string : str
        Path and name of the local file.
    """
    return clean(input_string, replace_by_none = '!@#$^&%*()+=-\/\[\]{}|:<>?,. ', strip_accents = 'ascii')


def print_data_summary(data, label):
    """Print the summary of a dataframe.
    Parameters
    ----------
    data : DataFrame
        DataFrame to be summarized.
    label : Name of the dataframe.
    """
    print('--- ' + label + ' ---')
    print(data.dtypes)
    print(data.head())
    print(data.shape)
    
    
def load_insee_data():
    """Load INSEE data.
    """
    # Lecture du fichier
    insee_pkl = '/media/ant/data_crypt/ds/open_data/deces/deces_compil/deces.pkl'

    with open(insee_pkl, 'rb') as input:
        insee_data = pickle.load(input)
        
    insee_data = insee_data.add_suffix('_insee')
    insee_data.index.names = ['index_insee']
    
    number_insee_rows = insee_data.shape[0]
    print('INSEE data : ' + str(number_insee_rows) + " rows")
    
    return insee_data  


def index(local_data, insee_data):
    '''Indexation des enregistrements à comparer'''
    # Index
    print('# Index')
    indexer = recordlinkage.Index()
    #indexer.block('prenom', 'prenom1')
    #indexer.block('prenom', 'prenom12')
    indexer.block('nom_local', 'nom_insee')
    indexer.block('date_naissance_local', 'date_naissance_insee')
    candidates = indexer.index(local_data, insee_data)
    
    number_of_candidates = candidates.shape[0]
    print("Candidates : " + str(number_of_candidates) + " rows")
    number_of_local = candidates.index_1.value_counts()
    
    #TODO:
    
    return candidates


def compare(candidates, local_data, insee_data):
    
    # Comparaison
    print('# Compare')
    compare = recordlinkage.Compare(njobs = -1)

    # Comparison criteria
    ## Nom
    compare.string('nom_local', 'nom_insee', method = 'damerau_levenshtein', label='comp_nom')
    ## Prénom
    compare.string('prenom_local', 'prenom1_insee', method = 'damerau_levenshtein', label='comp_prenom1')
    compare.string('prenom_local', 'prenom12_insee', method = 'damerau_levenshtein', label='comp_prenom12')
    ## DDN
    compare.string('date_naissance_local', 'date_naissance_insee', method = 'damerau_levenshtein', label='comp_date_naissance')

    # Comparison vectors
    compare_vectors = compare.compute(candidates, local_data, insee_data)
    compare_vectors["comp_prenom"] = compare_vectors[["comp_prenom1", "comp_prenom12"]].min(axis=1)
    compare_vectors["score_total"] = compare_vectors["comp_nom"] + compare_vectors["comp_prenom"] + compare_vectors["comp_date_naissance"]
    #compare_vectors["seuil"] = pd.to_numeric(compare_vectors["score_total"] >= threshold
    
    return compare_vectors


def select(matches, maximal_allowed_distance):
    print('# Select')
    selected_matches = matches[
        (matches["comp_nom"] <= maximal_allowed_distance) &
        (matches["comp_prenom"] <= maximal_allowed_distance) & 
        (matches["comp_date_naissance"] <= maximal_allowed_distance)
    ]
    selected_matches = selected_matches.reset_index()
    
    number_of_selected_matches = selected_matches.shape[0]
    print("Selected matches : " + str(number_of_selected_matches) + " rows")
    return selected_matches


def export(selected_matches, export_file, local_data, insee_data) :
    
    print('# Export')
    # Merge avec la première source de données
    matches_data_1 = local_data.merge(selected_matches, how='left', left_on='index_local', right_on='index_local')
    
    # Merge avec la deuxième source de données
    matches_data_2 = matches_data_1.merge(insee_data, how='left', left_on='index_insee', right_on='index_insee')
    
    cols = ['id_local', 'nom_src_local', 'nom_src_insee', 'prenom_src_local', 'prenoms_insee', 'date_naissance_local', 
            'date_naissance_insee', 'date_deces_local', 'date_deces_insee']
    matches_data_2[cols].to_csv(export_file, sep=";")