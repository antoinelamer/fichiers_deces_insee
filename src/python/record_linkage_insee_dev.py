
import pandas as pd
import numpy
try:
    import cPickle as pickle
except ModuleNotFoundError:
    import pickle
import re


def clean_string(s):
    s = str(s)
    s = s.lower()
    #s = s.replace(r'(\[.*?\]|\(.*?\)|\{.*?\})', '')
    s = re.sub('[!@#$^&%*()+-:<>?,. 0-9]', '', s)
    s = re.sub('[\\\\]', '', s)
    #s = s.replace(r'\s\s+', ' ')
    #s = s.lstrip().str.rstrip()
    return s


def load_insee_file():
	insee_pkl = '/media/ant/data_crypt/ds/open_data/deces/deces_compil/deces.pkl'

	with open(insee_pkl, 'rb') as input:
		df_insee = pickle.load(input)
	df_insee['index_2'] = numpy.arange(len(df_insee))

	return df_insee


def load_local_file(file):
	df_local = pd.read_csv(file, sep = ";")

	df_local['prenom'] = df_local['prenom'].apply(clean_string)
	df_local['nom'] = df_local['nom'].apply(clean_string)
	df_local['index_1'] = numpy.arange(len(df_local))

	return df_local


def select_candidates(df_local, df_insee):
    candidates_nom = index(df_local, df_insee, 'nom', 'nom')
    candidates_date_naissance = index(df_local, df_insee, 'date_naissance', 'date_naissance')

    # Concatenate
    candidates = [candidates_nom, candidates_date_naissance]
    print(candidates_nom.shape)
    print(candidates_date_naissance.shape)
    candidates = pd.concat(candidates)
    print(candidates.shape)
    candidates.drop_duplicates(inplace=True)
    print(candidates.shape)
    
    return candidates


def index(df_local, df_insee, blocking_field_1, blocking_field_2):
    pairs_df = df_local.merge(df_insee, left_on=blocking_field_1, right_on=blocking_field_2)[['index_1', 'index_2']]
    return pairs_df